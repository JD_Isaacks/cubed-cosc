#pragma once
#include "cube.h"
#include <cmath>
#include <fstream>
#include <istream>

void checkSafe(const int& current, int& lives, bool& end, Cube& cubed);
int directions(Cube& cubed, const int cur);
int directionsMenu(int fwrd, int back, int left, int right, int up, int down);
void gameEnd();
void gameLoop(int& choice, int cRoot, std::istream& inSafe, std::istream& inUnsafe);
int gameMenu(int& choice);
int gameSize();
Cube getCube(int size, std::istream& inSafe, std::istream& inUnsafe);
bool fileCheck(const std::istream& infile);
bool fileTest(const std::istream& inSafe, const std::istream& inUnsafe);
int playAgain();

