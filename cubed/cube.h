#ifndef CUBE_H
#define CUBE_H
#pragma once

#include <algorithm>
#include <ctime>
#include <fstream>
#include <istream>
#include <iostream>
#include <numeric>
#include <map>
#include <string>
#include <sstream>
#include <vector>

// Global enum for cube movement
const enum directions { UP = 1, DOWN, FWRD, BACK, RGHT, LEFT, EXIT };

class Room;
/**
* <p>
* This class creates a cube object to hold
* a set of rooms. Future use will include
* cube settings such as difficulty.
* </p>
*
* @author James Isaacks
* @since 0.1
*/
class Cube
{
public:
	Cube();
	Cube(int root, int sqrd, int size, std::istream& inSafe, std::istream& inUnsafe);
	~Cube();

	int getCSquare();
	std::string getDescpADJ(int adj);
	std::string getDescpCUR(int cur);
	int findAdjacent(int direction, int location);
	bool Cube::isSafe(int cur);
	int moveRoom(int adj);

private:

	int cRoot;
	int cSize;
	int cSqrd;
	int *gameCube;
	int *faceBack;
	int *faceFrwd;
	std::istream& inSafe;
	std::istream& inUnsafe;
	std::vector<std::vector <std::string> > safeDescrp;
	std::vector<std::vector <std::string> > unsafeDescrp;
	std::map<int, Room*> rooms;

	int findRoom(int room);
	void getBACK();
	void getFaces();
	void getFRWD();
	void readFiles();
	void readSafe();
	void readUnsafe();
	void makeRooms();
	void randomizeCube();
};

#endif