/****************************
*			CUBED			*
*	A short, infuriating	*
*	game where players		*
*	attempt to get through	*
*	a cube similar to the	*
*	movie.					*
*							*
*	Written by James Isaacks*
*	& Mireya Carino.		*
*	COSC 1337, San Jacinto	*
*	South.					*
*							*
*	Last Edited:			*
*		Aug 5th, 2017		*
****************************/

#include "cube.h"
#include "cubeFuncs.h"

int main()
{
	// Open files for use
	std::ifstream inSafe;
	std::ifstream inUnsafe;
	inSafe.open("safe.txt");
	inUnsafe.open("unsafe.txt");
	// Test files for exception handling.
	if (fileTest(inSafe, inUnsafe) == false)
	{
		return 2;
	}

	int choice = 0;
	int cRoot;

	// This will loop the game until the player chooses to stop.
	do
	{
		cRoot = gameMenu(choice);
		gameLoop(choice, cRoot, inSafe, inUnsafe);
		choice = playAgain();
	} while (choice != 4);
	return 0;
}