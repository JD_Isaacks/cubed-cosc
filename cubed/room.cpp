// Room Class
#include "room.h"

Room::Room()
{
}

Room::Room(bool safe, std::string adj, std::string cur)
{
	isSafe = safe;
	adjacent = adj;
	current = cur;
}

Room::~Room()
{
}

// Returns room description when room is adjacent.
std::string Room::getDescpAdj()
{
	return adjacent;
}

// Returns room description when room is current.
std::string Room::getDescpCur()
{
	return current;
}

// If room is safe, returns true instead of false
bool Room::getIsSafe()
{
	if (isSafe == true)
		return true;
	else return false;
}