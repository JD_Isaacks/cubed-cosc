#ifndef ROOM_H
#define ROOM_H
#pragma once
#include <string>

/**
* <p>
* This class creates rooms.
* </p>
*
* @author James Isaacks
* @since 0.1
*/
class Room
{
public:
	Room();
	Room(bool safe, std::string adj, std::string cur);
	~Room();

	// Returns whether room is safe or not
	bool getIsSafe();
	// Returns room description when room is adjacent.
	std::string getDescpAdj();
	// Returns room description when room is current.
	std::string getDescpCur();

private:
	bool isSafe;
	std::string adjacent;
	std::string current;
};

#endif // !ROOM_H
