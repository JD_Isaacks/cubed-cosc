// Cube class
#include "cube.h"
#include "room.h"

Cube::Cube() : inSafe(inSafe), inUnsafe(inUnsafe)
{
}

// Creates a gameCube complete with rooms and room descriptions
Cube::Cube(int root, int sqrd, int size, std::istream& inSafe, std::istream& inUnsafe) : cSize(size), gameCube{ new int[cSize + 1] }, inSafe(inSafe), inUnsafe(inUnsafe)
{
	cRoot = root;
	cSqrd = sqrd;
	std::iota(gameCube, gameCube + cSize, 0);
//	for (int i = 0; i < cSize; ++i)
//	{
//		std::cout << gameCube[i] << "\n";
//	}
	
	readFiles();
	makeRooms();
	getFaces();
}

Cube::~Cube()
{
	delete[] gameCube;
}

// Retrieves Cube Squared
int Cube::getCSquare()
{
	return cSqrd;
}

// Display the adjacent description
std::string Cube::getDescpADJ(int adj)
{
	int id = gameCube[adj];
	Room temp = *rooms.at(id);
	return temp.getDescpAdj();
}

// Display the adjacent description
std::string Cube::getDescpCUR(int cur)
{
	int id = gameCube[cur];
	Room temp = *rooms.at(id);
	return temp.getDescpCur();
}

// Find location of current room in the cube
int Cube::findAdjacent(int direction, int location)
{
	int adj = 0;
	int loc = location; // error correct for starting position.
	switch (direction)
	{
	case UP:
	{
		adj = loc - cSqrd;
		if (adj <= 0)
			return 0;
		else return adj;
		break;
	}
	case DOWN:
	{
		adj = loc + cSqrd;
		if (adj > cSize - 1)
			return 0;
		else return adj;
		break;
	}
	case FWRD:
	{
		adj = loc - cRoot;
		if (faceFrwd + cSqrd != std::find(faceFrwd, faceFrwd + cSqrd, location))
			return 0;
		return adj;
		break;
	}
	case BACK:
	{
		adj = loc + cRoot;
		if (faceBack + cSqrd != std::find(faceBack, faceBack + cSqrd, location))
			return 0;
		return adj;
		break;
	}
	case LEFT:
	{
		adj = location - 1;
		if ((location - 1) % 5)
			return 0;
		return adj;
		break;
	}
	case RGHT:
	{
		adj = loc + 1;
		if (loc % 5)
			return 0;
		return adj;
		break;
	}
	default:
	{
		return 0;
	}
	}
}

void Cube::getBACK()
{
	faceBack = new int[cSqrd];
	int j = 0;
	for (int i = 0; i <= cSize; ++i)
	{
		faceBack[j] = i;
		if ((i + 1) % cRoot)
			i += cSqrd;
		++j;
	}
}

// Builds arrays to identify the front and back faces of the cube.
void Cube::getFaces()
{
	getFRWD();
	getBACK();
}

void Cube::getFRWD()
{
	faceFrwd = new int[cSqrd];
	int j = 0;
	for (int i = (cSqrd - cRoot); i <= cSize; ++i)
	{
		faceFrwd[j] = i;
		if ((i + 1) % cRoot)
			i += cSqrd;
		++j;
	}
}

// Find id number of room based on current location
int Cube::findRoom(int room)
{
	int i;
	for (i = 0; i < cSize; ++i)
	{
		if (gameCube[i] == room)
			return i;
	}
}

// Call current room to determine safe.
bool Cube::isSafe(int cur)
{
	int id = gameCube[cur];
	Room temp = *rooms.at(id);
	return temp.getIsSafe();
}

void Cube::readFiles()
{

	readSafe();
	readUnsafe();
	
}

void Cube::readSafe()

{
	std::vector<std::string> adjacent;
	std::vector<std::string> current;
	safeDescrp.push_back(adjacent);
	safeDescrp.push_back(current);
	std::string line;
	std::string adj;
	std::string cur;

	while (std::getline(inSafe, line))
	{
		std::istringstream ll(line);
		std::getline(ll, adj, '\t');
		safeDescrp.at(0).push_back(adj);
		std::getline(ll, cur);
		safeDescrp.at(1).push_back(cur);
	}
}

void Cube::readUnsafe()
{
	std::vector<std::string> adjacent;
	std::vector<std::string> current;
	unsafeDescrp.push_back(adjacent);
	unsafeDescrp.push_back(current);
	std::string line;
	std::string adj;
	std::string cur;

	while (std::getline(inUnsafe, line))
	{
		std::istringstream ll(line);
		std::getline(ll, adj, '\t');
		unsafeDescrp.at(0).push_back(adj);
		std::getline(ll, cur);
		unsafeDescrp.at(1).push_back(cur);
	}
}

// Creates all needed rooms of the cube and stores them.
void Cube::makeRooms()
{
	srand(static_cast<unsigned int>(time(0)));
	int j = 0;
	std::string adj;
	std::string cur;
	int size_safe = safeDescrp.at(0).size();
	int size_unsafe = unsafeDescrp.at(0).size();
	rooms.insert(std::make_pair(0, new Room(true, "a white, brightly lit room.", "There is nothing in the room and it almost seems as if\nthis could be the room you started in.\n")));

	for (int i = 1; i < cSize; ++i)
	{
		bool isSafe = rand() % 2;
		if (isSafe == true)
		{
			j = rand() % size_safe;
			adj = safeDescrp[0][j];
			cur = safeDescrp[1][j];
		}
		else
		{
			j = rand() % size_unsafe;
			adj = unsafeDescrp[0][j];
			cur = unsafeDescrp[1][j];
		}
		rooms.insert(std::make_pair(i, new Room(isSafe, adj, cur)));
	}
}

// Moves the player to chosen room
int Cube::moveRoom(const int adj)
{
	srand(static_cast<unsigned int>(time(0)));
	int id = gameCube[adj];
	if (rand() % 2 == 0)
	{
		randomizeCube();
		return findRoom(id);
	}
	else return adj;
}

// Randomizes the game cube
void Cube::randomizeCube()
{
	std::random_shuffle(&gameCube[0], &gameCube[cSize - 1]);
}