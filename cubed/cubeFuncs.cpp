#include "cubeFuncs.h"

void checkSafe(const int& current, int& lives, bool& end, Cube& cubed)
{
	bool isSafe = cubed.isSafe(current);
	if (isSafe == false)
	{
		lives -= 1;
		if (lives == 0)
		{
			end = true;
			std::cout << "You never saw it coming, at least it was quick.\n" << std::endl;
		}
	}
	return;
}

int directions(Cube& cubed, const int cur)
{
	int fwrd = cubed.findAdjacent(FWRD, cur);
	if (fwrd == 0);
	else
	{
		std::cout << "Through the door ahead, you see " << cubed.getDescpADJ(fwrd) << "\n";
	}
	int back = cubed.findAdjacent(BACK, cur);
	if (back == 0);
	else
	{
		std::cout << "Through the door behind you, you see " << cubed.getDescpADJ(back) << "\n";
	}
	int left = cubed.findAdjacent(LEFT, cur);
	if (left == 0);
	else
	{
		std::cout << "Through the door to the left of you, you see " << cubed.getDescpADJ(left) << "\n";
	}
	int right = cubed.findAdjacent(RGHT, cur);
	if (right == 0);
	else
	{
		std::cout << "Through the door to the right of you, you see " << cubed.getDescpADJ(right) << "\n";
	}
	int up = cubed.findAdjacent(UP, cur);
	if (up == 0);
	else
	{
		std::cout << "Through the door above you, you see " << cubed.getDescpADJ(up) << "\n";
	}
	int down = cubed.findAdjacent(DOWN, cur);
	if (down == 0);
	else
	{
		std::cout << "Through the door in the floor, you see " << cubed.getDescpADJ(down) << "\n" << std::endl;
	}

	// returns the location of the chosen direction by calling the direction menu.
	int dir = directionsMenu(fwrd, back, left, right, up, down);
	if (dir == EXIT)
		return -1;
	return cubed.findAdjacent(dir, cur);
}

// Asks player which direction they want to go.
int directionsMenu(int fwrd, int back, int left, int right, int up, int down)
{
	int index = 1;
	int choice = 0;
	do
	{
		std::cout << "\nWhich way do you go?\n";
		if (fwrd == 0);
		else
		{
			std::cout << index << ": Front\t";
			fwrd = index;
			++index;
		}
		if (back == 0);
		else
		{
			std::cout << index << ": Behind\t";
			back = index;
			++index;
		}
		if (left == 0);
		else
		{
			std::cout << index << ": Left\t";
			left = index;
			++index;
		}
		if (right == 0);
		else
		{
			std::cout << index << ": Right\t";
			right = index;
			++index;
		}
		if (up == 0);
		else
		{
			std::cout << index << ": Up\t";
			up = index;
			++index;
		}
		if (down == 0);
		else
		{
			std::cout << index << ": Down\t";
			down = index;
			++index;
		}
		std::cout << index << ": Exit\n";
	} while (!(std::cin >> choice) || (choice < 1) || (choice > 7));
	if (choice == back)
		return FWRD;
	else if (choice == back)
		return BACK;
	else if (choice == back)
		return LEFT;
	else if (choice == back)
		return RGHT;
	else if (choice == back)
		return UP;
	else if (choice == back)
		return BACK;
	else return EXIT;
}

void gameEnd()
{
	std::cout << "Looking closer, you think this might actully be the room you started in.\n"
		"As you are opening doors, you find that the one in the floor doesn't go to a room.\n"
		"\nIs it possible you found the way out?\n\n"
		"Going through the doorway, you find yourself dropped into a short hallway.\n"
		"You start to pass out but, before you do.. you think you see people.\n"
		"Amazingly, you made it through the CUBE.\nBut is this the end...\n\n... or the beginning?\n" << std::endl;
}

void gameLoop(int& choice, int cRoot, std::istream& inSafe, std::istream& inUnsafe)
{
	srand(static_cast<unsigned int>(time(0)));

	Cube cubed = getCube(cRoot, inSafe, inUnsafe);
	int adjacent = 0;
	int current = rand() % cubed.getCSquare();
	bool end = false;
	int lives = 6;
	int zero = ((cRoot * cRoot * (cRoot - 1) + floor((cRoot * cRoot) / 2)));

	while (end == false)
	{
		checkSafe(current, lives, end, cubed);
		if (lives != 0)
		{
			std::cout << lives - 1 << " other people remaining.\n";
			adjacent = directions(cubed, current);
			if (adjacent == -1)
			{
				end = true;
				break;
			}
			else if (adjacent == zero)
			{
				gameEnd();
				end = true;
			}
			current = cubed.moveRoom(adjacent);
			std::cout << cubed.getDescpCUR(current) << "\n" << std::endl;
		}
		else end = true;
	}
}

int gameMenu(int& choice)
{
	int size = 5;
	std::cout <<
		"You find yourself in a brightly lit, white room.\n"
		"The room looks like a cube, maybe ten foot by ten foot and ten feet tall.\n"
		"There are what look like hatches in the middle of each wall,\n"
		"as well as the ceiling and the floor.\n\n"
		"With you are five other people who are also just waking up.\n\n"
		"Welcome to the CUBE...\n" << std::endl;
	std::cout << "1: Change CUBE size\n2: Explore the Cube\n3: Exit\n" << std::endl;

	while (!(std::cin >> choice))
	{
		std::cin.clear();
		std::cin.ignore(99999999, '\n');
		std::cout << "Invalid entry." << std::endl;
	}

	switch (choice)
	{
	case 1:
		size = gameSize();
		return size;
		break;
	case 2:
		return 5;
		break;
	case 3:
		choice = 4;
		return 0;
	}
}

int gameSize()
{
	int cRoot = 0;
	system("cls");
	std::cout << "Size must be an odd number.\nEnter size: (default: 5)\n" << std::endl;
	do
	{
		while (!(std::cin >> cRoot))
		{
			std::cin.clear();
			std::cin.ignore(99999999, '\n');
			std::cout << "Invalid entry." << std::endl;
		}
	} while (cRoot % 2 == 0);
	if (cRoot >= 11)
	{
		char yes;
		while (!(std::cin >> yes))
		{
			std::cout << "Cubes this large are almost imposible to win.\nWould you like to change the size? (y/n)\n";
		}
		if (yes == 'Y' || yes == 'y')
			gameSize();
		else return cRoot;
	}
	return cRoot;
}

Cube getCube(int size, std::istream& inSafe, std::istream& inUnsafe)
{
	int root = size;
	int sqrd = pow(size, 2);
	int csize = pow(size, 3);
	return Cube(root, sqrd, csize, inSafe, inUnsafe);
}

bool fileCheck(const std::istream& infile)
{
	if (!infile)
	{
		std::cout << "No file found\n";
		system("pause");
		return false;
	}
	else return true;
}

bool fileTest(const std::istream& inSafe, const std::istream& inUnsafe)
{
	// Test files for exception
	if (fileCheck(inSafe) == false)
		return false;
	else if (fileCheck(inUnsafe) == false)
		return false;
	else return true;
}

int playAgain()
{
	char again;
	std::cout << "Whould you like to try again? (y,n)\n" << std::endl;
	while (!(std::cin >> again) || ((again != 'Y') && (again != 'y') && (again != 'N') && (again != 'n')))
	{
		std::cout << "Invalid answer. Would you like to try again? (y/n)\n" << std::endl;
	}
	if (again == 'N' || again == 'n') return 4;
	return 0;
}
